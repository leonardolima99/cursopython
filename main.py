# Hello World Kivy

from kivy.app import App
from kivy.uix.button import Button

class PrimeiroApp(App):
    def build(self):
        return Button(text='Hello World Kivy!')

PrimeiroApp().run()
